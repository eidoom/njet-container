FROM docker.io/library/debian:buster-slim AS builder

RUN apt update && \
        apt install -y wget g++ gfortran make python 

WORKDIR /usr/local/src/

RUN wget https://bitbucket.org/njet/njet/downloads/njet-2.1.1.tar.gz && \
        tar -xzf njet-2.1.1.tar.gz && \ 
        rm njet-2.1.1.tar.gz && \
        mkdir njet-2.1.1/build

WORKDIR /usr/local/src/njet-2.1.1/build

RUN FFLAGS='-std=legacy' CXXFLAGS='-std=c++11 -O2' ../configure --prefix=/usr/local/njet --enable-quaddouble && \
        make -j && \
        make check && \
        make install

FROM docker.io/library/debian:buster-slim

RUN apt update && \
        apt install -y python

COPY --from=builder /usr/local/njet/bin/* /usr/local/bin
COPY --from=builder /usr/local/njet/include/* /usr/local/include
COPY --from=builder /usr/local/njet/lib/* /usr/local/lib
COPY --from=builder /usr/local/njet/share/* /usr/local/share
