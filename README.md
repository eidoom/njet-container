# [njet-container](https://gitlab.com/eidoom/njet-container)

Image for [NJet](https://bitbucket.org/njet/njet/).

## Get

Container image on [Docker Hub](https://hub.docker.com/r/eidoom/njet).

Get it with
```shell
docker pull eidoom/njet
```

## Use

Interactive:
```shell
docker run -it --rm eidoom/njet bash
```

Alternative usage discussed on the [Rivet website](https://rivet.hepforge.org/trac/wiki/Docker) could be similarly used for NJet.

## Development

See:
* https://gitlab.com/eidoom/njet-test-container
* https://gitlab.com/eidoom/njet-git-container
